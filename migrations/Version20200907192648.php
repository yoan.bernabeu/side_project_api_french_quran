<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200907192648 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Make relation';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE verse ADD surah_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE verse ADD CONSTRAINT FK_D2F7E69F7C87876F FOREIGN KEY (surah_id) REFERENCES surah (id)');
        $this->addSql('CREATE INDEX IDX_D2F7E69F7C87876F ON verse (surah_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE verse DROP FOREIGN KEY FK_D2F7E69F7C87876F');
        $this->addSql('DROP INDEX IDX_D2F7E69F7C87876F ON verse');
        $this->addSql('ALTER TABLE verse DROP surah_id');
    }
}
