<?php

namespace App\Entity;

use App\Repository\VerseRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VerseRepository::class)
 */
class Verse
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $position;

    /**
     * @ORM\Column(type="text")
     */
    private $frenchText;

    /**
     * @ORM\Column(type="text")
     */
    private $arabicText;

    /**
     * @ORM\Column(type="integer")
     */
    private $positionInSurah;

    /**
     * @ORM\Column(type="integer")
     */
    private $juz;

    /**
     * @ORM\Column(type="integer")
     */
    private $hizb;

    /**
     * @ORM\ManyToOne(targetEntity=Surah::class, inversedBy="verses")
     */
    private $surah;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getFrenchText(): ?string
    {
        return $this->frenchText;
    }

    public function setFrenchText(string $frenchText): self
    {
        $this->frenchText = $frenchText;

        return $this;
    }

    public function getArabicText(): ?string
    {
        return $this->arabicText;
    }

    public function setArabicText(string $arabicText): self
    {
        $this->arabicText = $arabicText;

        return $this;
    }

    public function getPositionInSurah(): ?int
    {
        return $this->positionInSurah;
    }

    public function setPositionInSurah(int $positionInSurah): self
    {
        $this->positionInSurah = $positionInSurah;

        return $this;
    }

    public function getJuz(): ?int
    {
        return $this->juz;
    }

    public function setJuz(int $juz): self
    {
        $this->juz = $juz;

        return $this;
    }

    public function getHizb(): ?int
    {
        return $this->hizb;
    }

    public function setHizb(int $hizb): self
    {
        $this->hizb = $hizb;

        return $this;
    }

    public function getSurah(): ?Surah
    {
        return $this->surah;
    }

    public function setSurah(?Surah $surah): self
    {
        $this->surah = $surah;

        return $this;
    }
}
